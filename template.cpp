#include <stdio.h>

int main() {
    void getNum(int &);
    void getLine(char *);
    void getWord(char *);
    
}

void getNum(int &n) {
    n = 0;
    while (true) {
        char c = getchar_unlocked();
        if (c < '0' || c > '9')
            break;
        n = n * 10 + (c - 48);
    }
}

void getLine(char *c) {
    char t;
    int i = 0;
    while ((t = getchar_unlocked()) != -1) {
        if (t == '\n')
            break;
        c[i++] = t;
   }
   c[i] = '\0';
}

void getWord(char *c) {
    char t;
    int i = 0;
    while ((t = getchar_unlocked()) != -1) {
        if (t == '\n' || t == ' ')
            break;
        c[i++] = t;
   }
   c[i] = '\0';
}