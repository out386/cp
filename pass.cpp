#include <vector>
#include <stdio.h>

using namespace std;

   vector<char> pass;
   vector<char> buf;

int main() {
    int cases;
    pass.reserve(1001000);
    buf.reserve(1001000);
    void getNum(int &);
    getNum(cases);

    for (int i = 0; i < cases; i++) {
        char temp;
        while ((temp = getchar_unlocked()) != -1 && temp != '\n') {
            switch (temp) {
                case '<':
                    if (!pass.empty()) {
                        buf.push_back(pass.back());
                        pass.pop_back();
                    }
                    break;
                case '>':
                    if (!buf.empty()) {
                        pass.push_back(buf.back());
                        buf.pop_back();
                    }
                    break;
                case '-':
                    if (!pass.empty())
                        pass.pop_back();
                    break;
                default:
                    pass.push_back(temp);
            }

        }
        auto start = pass.begin();
        auto end = pass.end();
        while (start != end) {
            putchar_unlocked(*start);
            start++;
        }

        while(!buf.empty()) {
            putchar_unlocked(buf.back());
            buf.pop_back();
        }

        putchar_unlocked('\n');

        pass.clear();
        buf.clear(); 
    }
}

void getNum(int &n) {
    n = 0;
    while (true) {
        char c = getchar_unlocked();
        if (c < '0' || c > '9')
            break;
        n = n * 10 + (c - 48);
    }
}