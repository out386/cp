#include <iostream>
#include <unordered_map>

using namespace std;

int main() {
    long long n;
    int m;
    int i;
    long long temp;
    int minCount = 0;
    int numUniqueGifts = 0;
    unordered_map<long long, int> freq;

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    cin >> n >> m;

    for (i = 0; i < m; i++) {
        cin >> temp;
        auto vl = freq.find(temp);
        if (vl == freq.end()) {
            freq.insert(make_pair(temp, 1));
            numUniqueGifts++;
        } else {
            freq.erase(vl);
            vl -> second++;
            freq.insert(*vl);
        }
    };

    if (n > numUniqueGifts) {
        cout << (n - numUniqueGifts) << "\n";
        return 0;
    }

    int curMin = freq.begin() -> second;
    for (auto bg : freq) {
        if (bg.second < curMin) {
            curMin = bg.second;
            minCount = 1;
        } else if (bg.second == curMin)
            minCount++;
        //cout << bg.first << ":" << bg.second << " ";
    }
    cout << minCount << "\n";
}