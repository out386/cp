#include <cstring>
#include <set>
#include <iostream>
#include <iterator>

using namespace std;

struct Comp {
    bool operator()(const pair <char*, int>& a, const pair <char*, int>& b) const {
        if(a.second < b.second)
            return true;
        return false;
    }
};

set <pair<char*, int>, Comp> stocks;
const char delim[] = " ";
const char zero = '0';

int main () {
    void operate(char *, int);
    char instruction[24];
    int cases, i=1;
    
    std::ios::sync_with_stdio(false);
    cin.tie(NULL);
    cin >> cases;
    cin.get();

    for(i=1; i<=cases; i++) {
        cin.getline(instruction, 24);
        operate(&instruction[0], i);
    }
}

void operate(char *instruction, int day) {
    int type;
    void addS(char[], int);
    void changeS(char*, int);
    void buyS(int);
    
    type = instruction[0] - zero;
    instruction += 2;
    

    if (type == 1) {
        char *token = strtok(instruction, delim);
        char *name = token;
        token = strtok(NULL,delim);
        addS(name, atoi(token));
    } else if (type == 2) {
        char *token = strtok(instruction, delim);
        char *name = token;
        token = strtok(NULL,delim);
        changeS(name, atoi(token));
    } else
        buyS(day);
}

void addS(char *pName, int value) {
    char * name = (char*) malloc(11);
    strcpy(name, pName);
    stocks.insert(make_pair(name, value));
}

void changeS(char *name, int newValue) {
    set<pair<char*, int>, Comp>::iterator end = stocks.end();
    set<pair<char*, int>, Comp>::iterator stock;

    for (set<pair<char*, int>, Comp>::iterator it = stocks.begin(); it != end; it++) {
        if (strcmp(it -> first, name) == 0) {
            stock = it;
            break;
        }
    }
    stocks.erase(stock);
    pair<char*, int> elem = *stock;
    elem.second = newValue;
    stocks.insert(elem);
}

void buyS(int day) {
    set<pair<char*, int>, Comp>::iterator stock = stocks.begin();
    printf("%s %d\n", stock -> first, day);
    stocks.erase(stock);
}