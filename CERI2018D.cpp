#include <iostream>
#include <math.h>

using namespace std;

int main() {
    void factorize(long);
    
    int cases;

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    cin >> cases;
    for (int i = 0; i < cases; i++) {
        long num;
        cin >> num;
        factorize(num);
    }
}

void factorize(long num) {
    long i, srt;
    while (num > 2 && num % 2 == 0) {
        num /= 2;
    }
    srt = sqrt(num);
    for (i = 3; i <= srt; i = i + 2) {
        while (num % i == 0 && num != i) {
            num /= i;
        }

        srt = sqrt(num);
    }
    cout << num << "\n";
}