#include <iostream>
#include <math.h>

#define MOD 1000000007

using namespace std;

int main() {
    long long factorize(long long);
    int pow2(int);
    int cases, m;

    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    cin >> cases >> m;    
    long long price = 0;

    for (int i = 0; i < cases; i++) {
        int ni;
        long long fn;
        cin >> ni;
        fn = pow2(ni - 2);
        fn = (fn * m) % MOD;
        fn = fn + ni + m;
        fn = fn % MOD;
        price += factorize(fn);
    }

    cout << "The President needs to pay " << price << " dollar(s)\n";

}

long long factorize(long long num) {
    long long i, srt;
    while (num > 2 && num % 2 == 0) {
        num /= 2;
    }
    srt = sqrt(num);
    for (i = 3; i <= srt; i = i + 2) {
        while (num % i == 0 && num != i) {
            num /= i;
        }

        srt = sqrt(num);
    }
    return num;
}

int pow2(int num) {
    cout << "In:" << num;
    int p = 1;
    for (int i = 0; i < num; i++) {
        p *= 2;
        if (p >= MOD)
            p = p % MOD;
    }
    cout << "Out:" << p << "\n"; 
    return p;
}