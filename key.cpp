#include <iostream>
#include <cstring>

char delimeter[] = "|$ *@.&\\\"!^,? ";

using namespace std;

int main() {
    int cases, i;
    char line[101];
    void process(char*);

    std::ios::sync_with_stdio(false);
    cin >> cases;
    cin.get();
    for(i = 0; i < cases; i++) {
        cin.getline(line, 101);
        process(line);
    }
}

void process(char *line) {
    char *word;
    int i;
    bool foundWords = false;
    word = strtok(line, delimeter);
    while (word != NULL) {
        for (i=0; word[i] != '\0'; i++) {
            if (word[i] == '#') {
                printf("%s\n", word);
                foundWords = true;
                break;
            }
        }
        word = strtok(NULL, delimeter);
    }

    if (!foundWords)
        printf("No keywords.\n");
}